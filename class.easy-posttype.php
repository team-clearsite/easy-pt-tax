<?php

namespace CLS;
use CLS\Easy_Taxonomy;
require_once( __DIR__ .'/class.easy-taxonomy.php' );

load_theme_textdomain( 'easy-pt-tax', __DIR__ .'/lang' );

class Easy_PostType {
	private $key;
	private $single;
	private $multiple;

	private $supports = array( 'title', 'editor', 'author', 'yoast-seo' );
	private $taxonomies = array();
	private $hierarchical = false;
	private $public = true;
	private $rewrite = true;
	private $show_ui = true;
	private $show_in_menu = true;
	private $delete_with_user = false;
	private $menu_position = 5;
	private $show_in_admin_bar = true;
	private $show_in_nav_menus = true;
	private $can_export = true;
	private $has_archive = true;
	private $exclude_from_search = false;
	private $publicly_queryable = true;
	private $capability_type = 'page';

	function __construct( $key, $single, $multiple ) {
		$this->key      = $key;
		$this->single   = $single;
		$this->multiple = $multiple;
	}

	function _doRegistration() {
		$self = &$this;

		if (did_action('init')) {
			$self->register_post_type();
		}
		add_action( 'init', function () use ( $self ) {
			$self->register_post_type();
		} );
	}

	/**
	 * @return mixed
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @return mixed
	 */
	public function getSingle() {
		return $this->single;
	}

	/**
	 * @param mixed $single
	 *
	 * @return Easy_PostType
	 */
	public function setSingle( $single ) {
		$this->single = $single;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMultiple() {
		return $this->multiple;
	}

	/**
	 * @param mixed $multiple
	 *
	 * @return Easy_PostType
	 */
	public function setMultiple( $multiple ) {
		$this->multiple = $multiple;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getSupports() {
		return $this->supports;
	}

	/**
	 * @param string $support1
	 *
	 * @return Easy_PostType
	 */
	public function addSupport( $support1 /*, ..., $supportN */ ) {
		foreach ( func_get_args() as $support ) {
			// common typo
			if ('thumbnails' === $support) {
				$support = 'thumbnail';
			}
			$this->supports[] = $support;

			// automatically add theme support.
			if ('thumbnail' === $support) {
				add_theme_support('post-thumbnails', array( $this->getKey() ));
			}
		}
		$this->supports = array_unique( $this->supports );

		return $this;
	}

	/**
	 * @param string $support
	 *
	 * @return Easy_PostType
	 */
	public function removeSupport( $support1 /*, ..., $supportN */ ) {
		foreach (func_get_args() as $support) {
			// common typo
			if ('thumbnails' === $support) {
				$support = 'thumbnail';
			}
			$this->supports = array_combine( $this->supports, $this->supports );
			if ( isset( $this->supports[ $support ] ) ) {
				unset( $this->supports[ $support ] );
			}
			$this->supports = array_values( $this->supports );

			if ('thumbnail' === $support) {
				global $_wp_theme_features;
				if (isset($_wp_theme_features[ $support ]) && is_array($_wp_theme_features[ $support ]) && is_array($_wp_theme_features[ $support ][0])) {
					$_wp_theme_features[ $support ][0] = array_combine($_wp_theme_features[ $support ][0], $_wp_theme_features[ $support ][0]);
					if (isset($_wp_theme_features[ $support ][0][ $this->getKey() ])) {
						unset($_wp_theme_features[ $support ][0][ $this->getKey() ]);
					}
					$_wp_theme_features[ $support ][0] = array_values($_wp_theme_features[ $support ][0]);
				}
			}

		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getTaxonomies() {
		return $this->taxonomies;
	}

	/**
	 * @param string|Easy_Taxonomy $taxonomy
	 *
	 * @return Easy_PostType
	 */
	public function addTaxonomy( $taxonomy ) {
		if ( is_a( $taxonomy, Easy_Taxonomy::class ) ) {
			$taxonomy = $taxonomy->getKey();
		}
		$this->taxonomies[] = $taxonomy;
		$this->taxonomies   = array_unique( $this->taxonomies );

		return $this;
	}

	/**
	 * @param string|Easy_Taxonomy $taxonomy
	 *
	 * @return Easy_PostType
	 */
	public function removeTaxonomy( $taxonomy ) {
		if ( is_a( $taxonomy, Easy_Taxonomy::class ) ) {
			$taxonomy = $taxonomy->getKey();
		}
		$this->taxonomies = array_combine( $this->taxonomies, $this->taxonomies );
		if ( isset( $this->taxonomies[ $taxonomy ] ) ) {
			unset( $this->taxonomies[ $taxonomy ] );
		}
		$this->taxonomies = array_values( $this->taxonomies );

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isHierarchical() {
		return $this->hierarchical;
	}

	/**
	 * @param bool $hierarchical
	 *
	 * @return Easy_PostType
	 */
	public function setHierarchical( $hierarchical ) {
		$this->hierarchical = ! ! $hierarchical;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPublic() {
		return $this->public;
	}

	/**
	 * @param bool $public
	 *
	 * @return Easy_PostType
	 */
	public function setPublic( $public ) {
		$this->public = ! ! $public;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function getRewrite() {
		return $this->rewrite;
	}

	/**
	 * @return Easy_PostType
	 */
	public function enableRewriteDefault() {
		$this->rewrite = true;

		return $this;
	}

	/**
	 * @param string|array $slug The slug to use for this post_type, set NULL for default value. If you supply 2 slugs
	 *                           by way of array, the first is for the singles, the second is for the archive page
	 * @param bool $pages Enable pagination for this post-type
	 * @param bool $with_front Add default permalink structure; WP default = true, our default is FALSE!
	 * @param bool $feeds Add support for feeds, set false or true, leave NULL for default (same as has_archive)
	 *
	 * @return Easy_PostType
	 */
	public function enableRewriteSpecial( $slug = null, $pages = true, $with_front = false, $feeds = null ) {
		$archive_slug = false;
		if (is_array($slug)) {
			list($slug, $archive_slug) = $slug;
			if ($archive_slug == $slug) $archive_slug = false;
		}
		$this->rewrite = array(
			'slug'       => ( null === $slug || ! $slug ) ? $this->getKey() : $slug,
			'with_front' => true !== $with_front ? false : true,
			'pages'      => $pages,
			'feeds'      => ( null === $feeds ) ? $this->hasArchive() : $feeds,
		);

		if ($archive_slug) {
			$this->has_archive = $archive_slug;
		}

		return $this;
	}

	/**
	 * @param bool $rewrite
	 *
	 * Array can have
	 *
	 * @return Easy_PostType
	 */
	public function disableRewrite() {
		$this->rewrite = false;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowUi() {
		return $this->show_ui;
	}

	/**
	 * @param bool $show_ui
	 *
	 * @return Easy_PostType
	 */
	public function setShowUi( $show_ui ) {
		$this->show_ui = ! ! $show_ui;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowInMenu() {
		return $this->show_in_menu;
	}

	/**
	 * @param bool $show_in_menu
	 *
	 * @return Easy_PostType
	 */
	public function setShowInMenu( $show_in_menu ) {
		$this->show_in_menu = ! ! $show_in_menu;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isDeleteWithUser() {
		return $this->delete_with_user;
	}

	/**
	 * @param bool $delete_with_user
	 *
	 * @return Easy_PostType
	 */
	public function setDeleteWithUser( $delete_with_user ) {
		$this->delete_with_user = ! ! $delete_with_user;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getMenuPosition() {
		return $this->menu_position;
	}

	/**
	 * @param int $menu_position
	 *
	 * @return Easy_PostType
	 */
	public function setMenuPosition( $menu_position ) {
		$this->menu_position = $menu_position;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowInAdminBar() {
		return $this->show_in_admin_bar;
	}

	/**
	 * @param bool $show_in_admin_bar
	 *
	 * @return Easy_PostType
	 */
	public function setShowInAdminBar( $show_in_admin_bar ) {
		$this->show_in_admin_bar = ! ! $show_in_admin_bar;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowInNavMenus() {
		return $this->show_in_nav_menus;
	}

	/**
	 * @param bool $show_in_nav_menus
	 *
	 * @return Easy_PostType
	 */
	public function setShowInNavMenus( $show_in_nav_menus ) {
		$this->show_in_nav_menus = ! ! $show_in_nav_menus;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isCanExport() {
		return $this->can_export;
	}

	/**
	 * @param bool $can_export
	 *
	 * @return Easy_PostType
	 */
	public function setCanExport( $can_export ) {
		$this->can_export = ! ! $can_export;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function hasArchive() {
		return $this->has_archive;
	}

	/**
	 * @param bool $has_archive
	 *
	 * @return Easy_PostType
	 */
	public function setHasArchive( $has_archive ) {
		$this->has_archive = ! ! $has_archive;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isExcludeFromSearch() {
		return $this->exclude_from_search;
	}

	/**
	 * @param bool $exclude_from_search
	 *
	 * @return Easy_PostType
	 */
	public function setExcludeFromSearch( $exclude_from_search ) {
		$this->exclude_from_search = ! ! $exclude_from_search;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPubliclyQueryable() {
		return $this->publicly_queryable;
	}

	/**
	 * @param bool $publicly_queryable
	 *
	 * @return Easy_PostType
	 */
	public function setPubliclyQueryable( $publicly_queryable ) {
		$this->publicly_queryable = ! ! $publicly_queryable;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCapabilityType() {
		return $this->capability_type;
	}

	/**
	 * @return Easy_PostType
	 */
	public function setCapabilityAsPage() {
		$this->capability_type = 'page';

		return $this;
	}

	/**
	 * @return Easy_PostType
	 */
	public function setCapabilityAsPost() {
		$this->capability_type = 'post';
		$this->setHierarchical( false );

		return $this;
	}

	/**
	 * @return Easy_PostType
	 */
	public function setCapabilityAsSelf() {
		$this->capability_type = 'post';

		// capability tree

		return $this;
	}

	private function r( $text ) {
		return strtr($text, array(
			'%single%' => $this->single,
			'%multiple%' => $this->multiple,
			'%singular%' => $this->single,
			'%plural%' => $this->multiple,
			'%posttype%' => $this->single,
			'%posttypes%' => $this->multiple,
		));

	}

	public function register_post_type() {

		// support yoast seo
		if (!in_array('yoast-seo', $this->supports)) {
			// this is the default;
			// yoast seo will activate on all post types
			// so to disable it, we need a filter
			// but with easy-posttype, you just 'removeSuppport("yoast-seo")' :)
			$post_type = $this->getKey();
			add_action('add_meta_boxes', function () use ($post_type) {
				remove_meta_box('wpseo_meta', $post_type, 'normal');
			}, PHP_INT_MAX);
		}

		// retroactively add a post-type to a category
		// this is required if the post-type is registered before the category
		$taxonomies = $this->taxonomies;
		$self = &$this;
		add_filter( 'registered_taxonomy', function($taxonomy) use ($taxonomies, $self) {
		  if (in_array($taxonomy, $taxonomies)) {
		    global $wp_taxonomies;

		    $wp_taxonomies[ $taxonomy ]->object_type[] = $self->getKey();

		    $wp_taxonomies[ $taxonomy ]->object_type = array_filter(array_unique($wp_taxonomies[ $taxonomy ]->object_type));
		  }
		}, PHP_INT_MAX);


		$labels = array(
			'name'                  => $this->multiple,
			'singular_name'         => $this->single,
			'menu_name'             => $this->multiple,
			'name_admin_bar'        => $this->single,
			'archives'              => $this->r( __('%posttype% archive', 'easy-pt-tax') ),
			'attributes'            => $this->r( __('%posttype% attributes', 'easy-pt-tax') ),
			'parent_item_colon'     => $this->r( __('Parent %posttype%:', 'easy-pt-tax') ),
			'all_items'             => $this->r( __('All %posttypes%', 'easy-pt-tax') ),
			'add_new_item'          => $this->r( __('Add new %posttype%', 'easy-pt-tax') ),
			'add_new'               => $this->r( __('Add new', 'easy-pt-tax') ),
			'new_item'              => $this->r( __('New %posttype%', 'easy-pt-tax') ),
			'edit_item'             => $this->r( __('Edit %posttype%', 'easy-pt-tax') ),
			'update_item'           => $this->r( __('Update %posttype%', 'easy-pt-tax') ),
			'view_item'             => $this->r( __('View %posttype%', 'easy-pt-tax') ),
			'view_items'            => $this->r( __('View %posttypes%', 'easy-pt-tax') ),
			'search_items'          => $this->r( __('Search %posttypes%', 'easy-pt-tax') ),
			'not_found'             => $this->r( __('%posttype% not found', 'easy-pt-tax') ),
			'not_found_in_trash'    => $this->r( __('%posttype% not found in trash', 'easy-pt-tax') ),
			'featured_image'        => $this->r( __('Featured image', 'easy-pt-tax') ),
			'set_featured_image'    => $this->r( __('Set featured image', 'easy-pt-tax') ),
			'remove_featured_image' => $this->r( __('Remove featured image', 'easy-pt-tax') ),
			'use_featured_image'    => $this->r( __('Use featured image', 'easy-pt-tax') ),
			'insert_into_item'      => $this->r( __('Insert into %posttype%', 'easy-pt-tax') ),
			'uploaded_to_this_item' => $this->r( __('Uploaded to this %posttype%', 'easy-pt-tax') ),
			'items_list'            => $this->r( __('%posttypes% list', 'easy-pt-tax') ),
			'items_list_navigation' => $this->r( __('%posttypes% list navigation', 'easy-pt-tax') ),
			'filter_items_list'     => $this->r( __('Filter %posttypes% list', 'easy-pt-tax') ),
		);
		$args   = array(
			'label'               => $this->single,
			'description'         => $this->r( __('%posttype% description', 'easy-pt-tax') ),
			'labels'              => $labels,
			'supports'            => $this->supports,
			'taxonomies'          => $this->taxonomies,
			'hierarchical'        => $this->hierarchical,
			'public'              => $this->public,
			'show_ui'             => $this->show_ui,
			'show_in_menu'        => $this->show_in_menu,
			'menu_position'       => $this->menu_position,
			'show_in_admin_bar'   => $this->show_in_admin_bar,
			'show_in_nav_menus'   => $this->show_in_nav_menus,
			'can_export'          => $this->can_export,
			'has_archive'         => $this->has_archive,
			'exclude_from_search' => $this->exclude_from_search,
			'publicly_queryable'  => $this->publicly_queryable,
			'capability_type'     => $this->capability_type,
			'rewrite'             => $this->rewrite,
			'delete_with_user'    => $this->delete_with_user,
		);

		register_post_type( $this->key, $args );

	}
}
