��    *      l  ;   �      �     �     �     �     �     �          -     I     V     n     v     �     �     �     �     �     �     �     �          #     :  
   I  
   T  	   _     i     |     �     �     �     �     �     �     �          #     1     M     `     p     �  �  �     !	     4	     J	     g	  !   �	     �	     �	     �	     �	     �	     
     %
      6
     W
     h
  &   u
     �
     �
     �
     �
     �
               '     =     R     ^     p     ~  "   �     �     �      �      �          3     D     \     {     �     �              #                  &                                                   (      $   *            
                %           )   !                    	          "             '                     %posttype% archive %posttype% attributes %posttype% description %posttype% not found %posttype% not found in trash %posttypes% list %posttypes% list navigation %terms% list %terms% list navigation Add new Add new %posttype% Add new %term% Add or remove %terms% All %posttypes% All %terms% Choose from most used Edit %posttype% Edit %term% Featured image Filter %posttypes% list Insert into %posttype% New %posttype% New %term% No %terms% Not found Parent %posttype%: Parent %term% Parent %term%: Popular %terms% Remove featured image Search %posttypes% Search %terms% Separate %terms% with commas Set featured image Update %posttype% Update %term% Uploaded to this %posttype% Use featured image View %posttype% View %posttypes% View %term% Project-Id-Version: Easy PostType and Taxonomy
POT-Creation-Date: 2019-05-16 15:37+0200
PO-Revision-Date: 2019-05-16 15:47+0200
Last-Translator: Remon Pel <remon@clearsite.nl>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-SearchPath-0: .
 %posttype% archief %posttype% attributen Beschrijving bij %posttypes% %posttype% niet gevonden Geen %posttypes% in de prullenbak Lijst van %posttypes% Navigeer binnen %posttypes% Lijst van %terms% Navigeer binnen %terms% Nieuwe %singular% %posttype% toevoegen %term% toevoegen %terms% toevoegen of verwijderen Alle %posttypes% Alle %terms% Maak keuze uit meest gebruikte %terms% %posttype% bewerken %term% bewerken Uitgelichtte afbeelding %posttypes% filteren Invoegen in %posttype% Nieuwe %posttype% %term% toevoegen Geen %terms% gevonden %term% niet gevonden Valt onder: Valt onder %term% Ouder %term%: Populaire %terms% Uitgelichte afbeelding verwijderen Naar %posttype% zoeken Zoek naar %terms% %terms% komma-gescheiden opgeven Uitgelichte afbeelding instellen %posttype% bijwerken %term% bijwerken Uploaden bij %posttype% Gebruik uitgelichte afbeelding %posttype% bekijken %posttypes% bekijken %term% bekijken 