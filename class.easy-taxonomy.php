<?php

namespace CLS;
require_once( __DIR__ . '/class.easy-posttype.php' );

class Easy_Taxonomy {
	private $key;
	private $post_types = array();

	private $single; // singular_name
	private $multiple; // name

	private $hierarchical = false;
	private $rewrite = true;
	private $public = true;
	private $show_ui = true;
	private $show_admin_column = true;
	private $show_in_admin_bar = true;
	private $show_in_rest_api = true;
	private $show_tagcloud = true;

	function __construct( $key, $single, $multiple ) {
		$this->key      = $key;
		$this->single   = $single;
		$this->multiple = $multiple;
	}

	function _doRegistration() {
		$self = &$this;

		if ( did_action( 'init' ) ) {
			$self->register_taxonomy();
		}
		add_action( 'init', function () use ( $self ) {
			$self->register_taxonomy();
		} );
	}

	/**
	 * @return mixed
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @return mixed
	 */
	public function getSingle() {
		return $this->single;
	}

	/**
	 * @param mixed $single
	 *
	 * @return Easy_Taxonomy
	 */
	public function setSingle( $single ) {
		$this->single = $single;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMultiple() {
		return $this->multiple;
	}

	/**
	 * @param mixed $multiple
	 *
	 * @return Easy_Taxonomy
	 */
	public function setMultiple( $multiple ) {
		$this->multiple = $multiple;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isHierarchical() {
		return $this->hierarchical;
	}

	/**
	 * @param bool $hierarchical
	 *
	 * @return Easy_Taxonomy
	 */
	public function setHierarchical( $hierarchical ) {
		$this->hierarchical = ! ! $hierarchical;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPublic() {
		return $this->public;
	}

	/**
	 * @param bool $public
	 *
	 * @return Easy_Taxonomy
	 */
	public function setPublic( $public ) {
		$this->public = ! ! $public;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function getInRestApi() {
		return $this->show_in_rest_api;
	}

	/**
	 * @param bool $show_in_rest_api
	 *
	 * @return Easy_Taxonomy
	 */
	public function setInRestApi( $show_in_rest_api ) {
		$this->show_in_rest_api = $show_in_rest_api;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowUi() {
		return $this->show_ui;
	}

	/**
	 * @param bool $show_ui
	 *
	 * @return Easy_Taxonomy
	 */
	public function setShowUi( $show_ui ) {
		$this->show_ui = ! ! $show_ui;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowAdminColumn() {
		return $this->show_admin_column;
	}

	/**
	 * @param bool $show_admin_column
	 *
	 * @return Easy_Taxonomy
	 */
	public function setShowAdminColumn( $show_admin_column ) {
		$this->show_admin_column = ! ! $show_admin_column;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowInAdminBar() {
		return $this->show_in_admin_bar;
	}

	/**
	 * @param bool $show_in_admin_bar
	 *
	 * @return Easy_Taxonomy
	 */
	public function setShowInAdminBar( $show_in_admin_bar ) {
		$this->show_in_admin_bar = ! ! $show_in_admin_bar;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowTagcloud() {
		return $this->show_tagcloud;
	}

	/**
	 * @param bool $show_tagcloud
	 *
	 * @return Easy_Taxonomy
	 */
	public function setShowTagcloud( $show_tagcloud ) {
		$this->show_tagcloud = ! ! $show_tagcloud;

		return $this;
	}

	/**
	 * @param string|Easy_PostType $post_type
	 *
	 * @return Easy_Taxonomy
	 */
	public function addPostType( $post_type ) {
		if ( is_a( $post_type, Easy_PostType::class ) ) {
			$post_type = $post_type->getKey();
		}
		$this->post_types[] = $post_type;
		$this->post_types   = array_unique( $this->post_types );

		return $this;
	}

	/**
	 * @param string|Easy_PostType $post_type
	 *
	 * @return Easy_Taxonomy
	 */
	public function removePostType( $post_type ) {
		if ( is_a( $post_type, Easy_PostType::class ) ) {
			$post_type = $post_type->getKey();
		}
		$this->post_types = array_combine( $this->post_types, $this->post_types );
		if ( isset( $this->post_types[ $post_type ] ) ) {
			unset( $this->post_types[ $post_type ] );
		}
		$this->post_types = array_values( $this->post_types );

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRewrite() {
		return $this->rewrite;
	}

	/**
	 * @param bool $rewrite
	 *
	 * @return Easy_Taxonomy
	 */
	public function setRewrite( $rewrite ) {
		$this->rewrite = $rewrite;

		return $this;
	}

	/**
	 * @param string $slug The url-slug to use for this taxonomy instead of the taxonomy-slug, set false-ish for default value.
	 * @param bool $with_front Add default permalink structure; WP default = true, our default is FALSE!
	 * @param bool $hierarchical_urls true or false allow hierarchical urls (implemented in Version 3.1) - defaults to false
	 *
	 * @return Easy_Taxonomy
	 */
	public function enableRewriteSpecial( $slug = null, $with_front = true, $hierarchical_urls = false ) {
		$this->rewrite = array(
			'slug'         => $slug ? $slug : $this->getKey(),
			'with_front'   => true === $with_front,
			'hierarchical' => $this->hierarchical && $hierarchical_urls,
		);

		return $this;
	}

	private function r( $text ) {
		return strtr( $text, array(
			'%single%'   => $this->single,
			'%multiple%' => $this->multiple,
			'%singular%' => $this->single,
			'%plural%'   => $this->multiple,
			'%term%'     => $this->single,
			'%terms%'    => $this->multiple,
		) );

	}

	private function register_taxonomy() {

		// retroactively add a post-type to a category
		// this is required if the post-type is registered before the category
		$post_types = $this->post_types;
		$self       = &$this;
		add_filter( 'registered_taxonomy', function ( $taxonomy ) use ( $post_types, $self ) {
			if ( $taxonomy = $self->getKey() ) {
				global $wp_taxonomies;

				$wp_taxonomies[ $taxonomy ]->object_type = array_merge( $wp_taxonomies[ $taxonomy ]->object_type, $post_types );

				$wp_taxonomies[ $taxonomy ]->object_type = array_filter( array_unique( $wp_taxonomies[ $taxonomy ]->object_type ) );
			}
		}, PHP_INT_MAX );

		$labels = array(
			'name'                       => $this->multiple,
			'singular_name'              => $this->single,
			'menu_name'                  => $this->multiple,
			'all_items'                  => $this->r( __( 'All %terms%', 'easy-pt-tax' ) ),
			'parent_item'                => $this->r( __( 'Parent %term%', 'easy-pt-tax' ) ),
			'parent_item_colon'          => $this->r( __( 'Parent %term%:', 'easy-pt-tax' ) ),
			'new_item_name'              => $this->r( __( 'New %term%', 'easy-pt-tax' ) ),
			'add_new_item'               => $this->r( __( 'Add new %term%', 'easy-pt-tax' ) ),
			'edit_item'                  => $this->r( __( 'Edit %term%', 'easy-pt-tax' ) ),
			'update_item'                => $this->r( __( 'Update %term%', 'easy-pt-tax' ) ),
			'view_item'                  => $this->r( __( 'View %term%', 'easy-pt-tax' ) ),
			'separate_items_with_commas' => $this->r( __( 'Separate %terms% with commas', 'easy-pt-tax' ) ),
			'add_or_remove_items'        => $this->r( __( 'Add or remove %terms%', 'easy-pt-tax' ) ),
			'choose_from_most_used'      => $this->r( __( 'Choose from most used', 'easy-pt-tax' ) ),
			'popular_items'              => $this->r( __( 'Popular %terms%', 'easy-pt-tax' ) ),
			'search_items'               => $this->r( __( 'Search %terms%', 'easy-pt-tax' ) ),
			'not_found'                  => $this->r( __( 'Not found', 'easy-pt-tax' ) ),
			'no_terms'                   => $this->r( __( 'No %terms%', 'easy-pt-tax' ) ),
			'items_list'                 => $this->r( __( '%terms% list', 'easy-pt-tax' ) ),
			'items_list_navigation'      => $this->r( __( '%terms% list navigation', 'easy-pt-tax' ) ),
		);
		$args   = array(
			'labels'            => $labels,
			'hierarchical'      => $this->hierarchical,
			'public'            => $this->public,
			'show_ui'           => $this->show_ui,
			'show_in_rest'      => $this->show_in_rest_api,
			'show_admin_column' => $this->show_admin_column,
			'show_in_admin_bar' => $this->show_in_admin_bar,
			'show_tagcloud'     => $this->show_tagcloud,
			'rewrite'           => $this->rewrite,
		);
		register_taxonomy( $this->key, $this->post_types, $args );
	}
}
